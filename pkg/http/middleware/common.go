package middleware

import (
	"fmt"
	"net/http"
)

// SetPostToJSON - sets the http headers necessary to tell the request that we're dealing in JSON
func SetPostToJSON(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// tell the browser we're working with JSON
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	}
}

// EnsureRequestMethod - this ensures that the request method we desire for the endpoint is enforced
func EnsureRequestMethod(method string) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			// check the http method
			if r.Method != method {
				w.WriteHeader(http.StatusMethodNotAllowed)
				fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Method Not Allowed\"}", http.StatusMethodNotAllowed)
				return
			}
			next.ServeHTTP(w, r)
		}
	}
}

// Authenticate - middleware that wraps some functionality for authentication
func Authenticate(validator func(string) bool) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			// check that the security key for the request is correct
			apiKey := r.Header.Get("Authorization")
			if !validator(apiKey) {
				w.WriteHeader(http.StatusUnauthorized)
				fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Unauthorized Access\"}", http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		}
	}
}
