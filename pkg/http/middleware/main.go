package middleware

import "net/http"

// Middleware - Type alias for http.HandlerFunc
type Middleware func(next http.HandlerFunc) http.HandlerFunc

// MiddlewareChain - Groups Middleware Functions into a single Callable Middleware
func MiddlewareChain(mw ...Middleware) Middleware {
	return func(final http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			last := final
			for i := len(mw) - 1; i >= 0; i-- {
				last = mw[i](last)
			}
			last(w, r)
		}
	}
}
