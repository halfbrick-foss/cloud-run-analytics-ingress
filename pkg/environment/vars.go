package environemnt

import (
	"log"
	"os"
)

// EnsureEnv - bails out of the application if an environemnt variable is not set
func EnsureEnv(key string) string {
	p, exists := os.LookupEnv(key)
	if !exists {
		log.Fatalf("Environment Variable %v is not set\n", key)
	}
	return p
}
