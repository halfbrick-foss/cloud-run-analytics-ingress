// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    analyticsevent, err := UnmarshalAnalyticsevent(bytes)
//    bytes, err = analyticsevent.Marshal()

package schema

import (
	"encoding/json"

	"cloud.google.com/go/civil"
)

func UnmarshalAnalyticsevent(data []byte) (Analyticsevent, error) {
	var r Analyticsevent
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Analyticsevent) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

// Definition of a Generic Analytics Event compatible with Schema definitions in bigquery,
// this is modelled off the Firebase Analytics export to BigQuery with unused fields omitted
type Analyticsevent struct {
	AppInfo        AppInfo               `json:"app_info"`
	Device         Device                `json:"device"`
	EventDate      civil.Date            `json:"event_date"`
	EventName      string                `json:"event_name"`
	EventParams    []EventParamElement   `json:"event_params"`
	EventTimestamp int64                 `json:"event_timestamp"`
	Geo            Geo                   `json:"geo"`
	UserID         string                `json:"user_id"`
	UserProperties []UserPropertyElement `json:"user_properties"`
}

type AppInfo struct {
	FirebaseAppID string `json:"firebase_app_id"`
	ID            string `json:"id"`
	InstallSource string `json:"install_source"`
	InstallStore  string `json:"install_store"`
	Version       string `json:"version"`
}

type Device struct {
	AdvertisingID          string `json:"advertising_id"`
	Category               string `json:"category"`
	IsLimitedAdTracking    string `json:"is_limited_ad_tracking"`
	Language               string `json:"language"`
	MobileBrandName        string `json:"mobile_brand_name"`
	MobileMarketingName    string `json:"mobile_marketing_name"`
	MobileModelName        string `json:"mobile_model_name"`
	MobileOSHardwareModel  string `json:"mobile_os_hardware_model"`
	OperatingSystem        string `json:"operating_system"`
	OperatingSystemVersion string `json:"operating_system_version"`
	TimeZoneOffsetSeconds  string `json:"time_zone_offset_seconds"`
	VendorID               string `json:"vendor_id"`
}

// Utility JSON schema object allowing key value storage in bq records for single level data
// sets
type EventParamElement struct {
	Key   string          `json:"key"`
	Value EventParamValue `json:"value"`
}

type EventParamValue struct {
	DoubleValue float64 `json:"double_value"`
	FloatValue  float64 `json:"float_value"`
	IntValue    int64   `json:"int_value"`
	StringValue string  `json:"string_value"`
}

type Geo struct {
	City         string `json:"city"`
	Continent    string `json:"continent"`
	Country      string `json:"country"`
	Metro        string `json:"metro"`
	Region       string `json:"region"`
	SubContinent string `json:"sub_continent"`
}

// Utility JSON schema object allowing key value storage in bq records for single level data
// sets
type UserPropertyElement struct {
	Key   string            `json:"key"`
	Value UserPropertyValue `json:"value"`
}

type UserPropertyValue struct {
	DoubleValue float64 `json:"double_value"`
	FloatValue  float64 `json:"float_value"`
	IntValue    int64   `json:"int_value"`
	StringValue string  `json:"string_value"`
}
