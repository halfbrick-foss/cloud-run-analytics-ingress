module gitlab.com/halfbrick-foss/cloud-run-analytics-ingress

go 1.13

require (
	cloud.google.com/go v0.65.0
	cloud.google.com/go/bigquery v1.8.0
	cloud.google.com/go/firestore v1.3.0
	firebase.google.com/go v3.13.0+incompatible
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/chromedp/cdproto v0.0.0-20201009231348-1c6a710e77de
	github.com/chromedp/chromedp v0.5.3
	github.com/matryer/try v0.0.0-20161228173917-9ac251b645a2 // indirect
	google.golang.org/api v0.34.0
	gopkg.in/matryer/try.v1 v1.0.0-20150601225556-312d2599e12e
)

replace gitlab.com/halfbrick-foss/cloud-run-analytics-ingress v0.0.0 => ./
