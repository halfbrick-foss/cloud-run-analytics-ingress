package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"reflect"
	"strings"
	"sync"
	"time"

	"cloud.google.com/go/bigquery"
	firebase "firebase.google.com/go"
	"gitlab.com/halfbrick-foss/cloud-run-analytics-ingress/internal/schema"
	"gitlab.com/halfbrick-foss/cloud-run-analytics-ingress/pkg/http/middleware"
	"google.golang.org/api/option"
)

var projectID string
var datasetID string

var ctx context.Context
var bq *bigquery.Client
var table *bigquery.Table

type workerError struct {
	status int
	reason string
	event  schema.Analyticsevent
}

// AnalyticEvents - Payload Structure
type AnalyticEvents struct {
	Events []schema.Analyticsevent `json:"events"`
}

// ParseHTTPRequest - parse the payload
func ParseHTTPRequest(data []byte) (AnalyticEvents, error) {
	var r AnalyticEvents
	err := json.Unmarshal(data, &r)
	return r, err
}

// MarshalString - convert struct to JSON
func (r *AnalyticEvents) MarshalString() string {
	bytes, _ := json.Marshal(r)
	return string(bytes)
}

func handler(w http.ResponseWriter, r *http.Request) {
	// read the http body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error Reading Request body: %v", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Error Reading Request body\"}", http.StatusInternalServerError)
		return
	}
	// Unmarshal the body into struct
	request, err := ParseHTTPRequest(body)
	if err != nil {
		log.Printf("JSON sent to the server: %v\n", request.MarshalString())
		log.Printf("Error Reading JSON: %v\n", err)
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Error Reading JSON\"}", http.StatusInternalServerError)
		return
	}
	// make the wait group
	var wg sync.WaitGroup
	eventErrorChannel := make(chan workerError, len(request.Events)) // Declare a unbuffered channel
	// loop through to event docs we received
	wg.Add(len(request.Events))
	for _, evt := range request.Events {
		go func(evt schema.Analyticsevent, wg *sync.WaitGroup, errs chan workerError) {
			defer wg.Done()
			// prepare the item insert statement
			rows := []interface{}{
				evt,
			}
			// insert the item
			if err := table.Inserter().Put(ctx, rows); err != nil {
				e := workerError{status: http.StatusInternalServerError, reason: err.Error(), event: evt}
				errs <- e
				return
			}
			return
		}(evt, &wg, eventErrorChannel)
	}
	wg.Wait()
	close(eventErrorChannel) // Closes the channel

	eventErrors := chanToSlice(eventErrorChannel).([]workerError)
	if len(eventErrors) == len(request.Events) {
		log.Printf("Inserting Rows had Errors: %+v\n", eventErrors)
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Event Data is Incorrect\"}", http.StatusBadRequest)
		return
	}

	if len(eventErrors) < len(request.Events) && len(eventErrors) > 0 {
		log.Printf("Inserting Rows had Errors: %+v\n", eventErrors)
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Partial Event Errors, some records may not have persisted\"}", http.StatusAccepted)
		return
	}

	// successs! we did the publish concurrently!
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "{\"status\":\"%d\",\"message\":\"Data Inserted\"}", http.StatusOK)
}

func chanToSlice(ch interface{}) interface{} {
	chv := reflect.ValueOf(ch)
	slv := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(ch).Elem()), 0, 0)
	for {
		v, ok := chv.Recv()
		if !ok {
			return slv.Interface()
		}
		slv = reflect.Append(slv, v)
	}
}

// EventTableExists - checks if a table exists for the dataset
func EventTableExists(ctx context.Context, table *bigquery.Table) bool {
	// check if the table exists
	_, err := table.Metadata(ctx)
	return err == nil
}

// CreateEventTable - makes a Table for the given Event
func CreateEventTable(ctx context.Context, bq *bigquery.Client, table *bigquery.Table, schemaItem interface{}) error {
	log.Printf("Attempting To Create Table %v\n", table.TableID)
	schema, err := bigquery.InferSchema(schemaItem)
	if err != nil {
		return fmt.Errorf("error creating schema: %v", err)
	}
	expiry, err := time.ParseDuration("730h")
	if err != nil {
		return fmt.Errorf("error expiry duration: %v", err)
	}
	err = table.Create(ctx, &bigquery.TableMetadata{
		// make none of the columns required
		Schema: schema.Relax(),
		TimePartitioning: &bigquery.TimePartitioning{
			Expiration:             expiry * 18,
			RequirePartitionFilter: true,
			Field:                  "EventDate",
		},
	})
	if err != nil {
		return fmt.Errorf("error creating table: %v", err)
	}
	return nil
}

func main() {
	ctx = context.Background()
	var err error
	bq, err = bigquery.NewClient(ctx, os.Getenv("PROJECT_ID"))
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

	// get the desired table ref
	table = bq.Dataset(os.Getenv("DATASET")).Table(os.Getenv("TABLE"))
	exists := EventTableExists(ctx, table)
	if !exists {
		err := CreateEventTable(ctx, bq, table, schema.Analyticsevent{})
		if err != nil {
			log.Fatalf("Error creating Table: %s\n", err.Error())
		}
	}

	opt := option.WithCredentialsFile(os.Getenv("GOOGLE_AUTHENTICATION_APPLICATION_CREDENTIALS"))
	app, err := firebase.NewApp(ctx, nil, opt)
	if err != nil {
		log.Fatalf("error initializing Firebase app: %v\n", err)
	}

	// build our midleware in here
	middleware := middleware.MiddlewareChain(
		middleware.SetPostToJSON,
		middleware.EnsureRequestMethod(http.MethodPost),
		middleware.Authenticate(func(idToken string) bool {
			// helper function if we want to allow always authenticated access to the API
			if _, exists := os.LookupEnv("IS_LOCAL"); exists == true {
				return true
			}
			//  from here on out we behave a s normal
			client, err := app.Auth(ctx)
			if err != nil {
				return false
			}
			splitToken := strings.Split(idToken, "Bearer")
			if len(splitToken) != 2 {
				return false
			}
			idToken = strings.TrimSpace(splitToken[1])
			if _, err = client.VerifyIDToken(ctx, idToken); err != nil {
				return false
			}

			return true
		}),
	)
	// hook up the HTTP server
	http.HandleFunc("/", middleware(handler))
	log.Printf("Starting API at port %s\n", os.Getenv("PORT"))
	log.Fatal(http.ListenAndServe(fmt.Sprintf("0.0.0.0:%s", os.Getenv("PORT")), nil))
}
