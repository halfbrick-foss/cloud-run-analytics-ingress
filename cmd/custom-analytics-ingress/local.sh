#! /bin/bash

# clean the old container incase it was running already
docker rm custom-analytics-ingress

# we need to go up to the root of the project which for the CMD pattern is two levels
cd ../../

#  build the container for running your code locally
docker build \
--build-arg PORT=5000 \
--build-arg IS_LOCAL=true \
--build-arg PROJECT_ID=test-project \
--build-arg DATASET=custom_analytics \
--build-arg TABLE=events \
--build-arg GOOGLE_APPLICATION_CREDENTIALS=/app/credentials/bq.json \
--build-arg GOOGLE_AUTHENTICATION_APPLICATION_CREDENTIALS=/app/credentials/firebase.json \
-t halfbrick/custom-analytics-ingress:local \
-f ./cmd/custom-analytics-ingress/Dockerfile .

# run the container so we can test the code in its environemnt locally
docker run --name custom-analytics-ingress -p 5000:5000 halfbrick/custom-analytics-ingress:local